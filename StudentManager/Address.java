package nz.ac.massey.cs159272.ass1.id14154248;

public class Address {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		

	}
	
	public Address clone() throws CloneNotSupportedException {
		try{  Address clone = new Address();
			clone.town = this.town;
			clone.street = this.street;
			clone.houseNumber = this.houseNumber;
			clone.postCode = this.postCode;
			return clone;
	    } catch(Exception e) { 
	        return null; 
	    }
	}
	
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getPostCode() {
		return postCode;
	}
	public void setPostCode(int postCode) {
		this.postCode = postCode;
	}
	public int getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}
	private String town = null;
	private String street = null;
	private int postCode = 0;
	private int houseNumber = 0;
}
