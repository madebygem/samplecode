package nz.ac.massey.cs159272.ass1.id14154248;

import java.util.Date;

public class Student implements Cloneable {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	@Override
	public Student clone() throws CloneNotSupportedException {
		try{  Student clone = new Student();
			clone.course = this.course;
			clone.address = (Address) this.address.clone();
			clone.dateOfBirth = (Date) this.dateOfBirth.clone();
			return clone;
	    } catch(Exception e) { 
	        return null; 
	    }
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	private String name = null;
	private String fName = null;
	private String id = null;
	private Date dateOfBirth = null;
	private Course course = null;
	private Address address = null;
}
