package nz.ac.massey.cs159272.ass1.id14154248;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

public class StudentManager {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}


void save(List<Student> students, String fileName) throws IOException {
	FileOutputStream fos = new FileOutputStream(fileName);
	ObjectOutputStream out = new ObjectOutputStream(fos);
	for (Student student : students) {
	out.writeObject(student);
	}
	out.close();
}

java.util.List<Student> fetch(String fileName) throws IOException {
	FileInputStream readFrom = new FileInputStream(fileName);
	ObjectInputStream reader = new ObjectInputStream(readFrom);
	// how to break up each piece of information into pieces of data to instantiate into one student?
	name = (Student) reader.readObject();
}

void publish(List<Student> students,String fileName) throws IOException {
	File toHTML = new File(fileName);
	FileOutputStream html = new FileOutputStream(toHTML);
	for (Student student: students) {
		// I think I need to create bytes here??
		html.write(student);
	}
}
}