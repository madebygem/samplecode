### GEMMA WARD 14154248 ###


from cmd import Cmd
import os, store, pickle, hash, shutil, datetime

class mybackup(Cmd):

    myArchive = os.path.expanduser(os.path.join("~","Desktop","myArchive"))
    subdirectory = os.path.join(myArchive,"objects")
    indexFile = os.path.join(myArchive,"index.txt")
    logFile = os.path.join(myArchive,"log.txt")

    def do_mybackup(self, args):
        parameters = args.split()
        action = parameters[0]
        if len(parameters) > 1:
            arg = parameters[1]
        else:
            arg = None

        if action == "init":
            self.init()

        elif action == "store":
            self.store(arg)

        elif action == "list":
            self.list(arg)

        elif action == "test":
            self.test()

        elif action == "get":
            self.get(arg)

        elif action == "restore":
            self.restore(arg)

        elif args == "":
            print("No command given.")
        else:
            print("Command", args, "not recognised.")




    def init(self):
        if not os.path.exists(self.subdirectory) and not os.path.isfile(self.indexFile):
            os.makedirs(self.subdirectory)
            os.chdir(self.myArchive)
            indexFile = open("index.txt","w+")
            print("Initialised back up file 'myArchive' in", self.myArchive)
        else:
            print("myArchive already exists")




    def storeRecursive(self,directory,dir,dict,filesStored,alreadyStored):
    #recursively back up

    #if the directory list is empty, print stats and end
        if len(dir) == 0:
            pickle.dump(dict,open(self.indexFile,"wb"))
            print("Directory", os.path.basename(directory), "stored successfully")
            if len(filesStored) > 0:
                print("New files added:")
                for item in filesStored:
                    print(item)
                with open(self.logFile,"a") as log:
                    timestamp = datetime.datetime.now().strftime("%A %d %B %Y %I:%M%p")
                    log.write(timestamp + '\n')
                    log.write("New files added:"+'\n')
                    for item in filesStored:
                        log.write(item + '\n')
                    log.write('\n')
                log.close()
            if alreadyStored > 0:
                print(alreadyStored, "file(s) in", os.path.basename(directory), "skipped as previously stored.")

    #if not empty, store next item
        else:
            toSave = dir.pop()
            if not toSave.startswith("."):
                filePath = os.path.join(directory,toSave)
                fileHash = hash.createFileSignature(filePath)
                if toSave not in dict.keys():
                    #copy the file to 'myArchive/objects'
                    shutil.copyfile(os.path.join(directory,toSave), os.path.join(self.subdirectory,toSave))
                    #add its entry to the dictionary
                    dict.update({toSave:[fileHash,directory]})
                    #and add it to the list of files stored
                    filesStored.append(toSave)
                else:
                #file is already in the dictionary
                    alreadyStored += 1

            #recurse
            self.storeRecursive(directory,dir,dict,filesStored,alreadyStored)




    def remove(self,dir,args):
        keys = os.listdir(dir)
        currentDict = pickle.load(open(self.indexFile,"rb"))
        for key in keys:
            if key in currentDict:
                del currentDict[key]
        pickle.dump(currentDict,open(self.indexFile,"wb"))
        shutil.rmtree(dir)
        self.store(args)




    def store(self,args):
        if args == None:
            print("No directory given.")
        elif not os.path.isdir(args):
            print("Invalid directory.")
        elif len([f for f in os.listdir(args) if not f.startswith(".")]) == 0:
            print("Directory",args,"skipped as it contains 0 files.")
        else:
            if os.path.isdir(args): #if given dir is valid
                #if index file is not empty, unpack it's current entries
                if os.path.getsize(self.indexFile) > 0:
                    currentDict = pickle.load(open(self.indexFile,"rb"))
                #else start a new dictionary
                else:
                    currentDict = {}

                #create a list of files in the given directory
                dir = os.listdir(args)

                #subdirectories need to be stored separately
                folders = []
                for item in dir:
                    if os.path.isdir(os.path.join(args,item)):
                        folders.append(item)
                for folder in folders:
                    dir.remove(folder)
                #recursively store these files into objects
                self.storeRecursive(args,dir,currentDict,[],0)
                for folder in folders:
                    self.store(os.path.join(args,folder))
            else: #if given dir is not valid
                print("Invalid directory: ",args)




    def list(self,args):
        valid = []
        dir = os.listdir(self.subdirectory)
        for file in dir:
            fullFile = os.path.join(self.subdirectory,file)
            if os.path.isfile(fullFile):
                valid.append(fullFile)
        if args != None:
            valid = [entry for entry in valid if args in entry]
        if len(valid) != 0:
            for item in valid:
                print(item)
        else:
            print("No items matching given pattern.")





    def test(self):
        correctCount = []
        erroneousPaths = []
        misMatchedFiles = []
        #dictionary containing all index.txt entries
        dict = pickle.load(open(self.indexFile,"rb"))

        #list of all the full paths stored inside myArchive/objects
        objects = os.listdir(self.subdirectory)

        #cycle through index, check that all keys have corresponding entries in [list of objects]
        for key in dict.keys():
            for file in objects:
                if file == key: #if a match is found
                    correctCount.append(key) #log it
                    #check the hash matches the file content
                    contentHash = dict[key][0]
                    newHash = hash.createFileSignature(os.path.join(self.subdirectory,file))
                    if contentHash != newHash: #and if it doesn't,
                        misMatchedFiles.append(key) #log it
                else: #no match is found for this key
                    erroneousPaths.append(key) #log it

        #print stats
        testOutput = []
        timestamp = datetime.datetime.now().strftime("%A %d %B %Y %I:%M%p")
        print("Correct entries in archive index: ",len(correctCount))
        testOutput.append("Correct entries in archive index: "+ str(len(correctCount)))
        if len(misMatchedFiles) > 0:
            print("Files with mismatched content: ")
            testOutput.append("Files with mismatched content:")
            for item in misMatchedFiles:
                print(item)
                testOutput.append(item + '\n')
        if len(dict.keys()) != len(correctCount):
            print("Index entries without matching files: ")
            testOutput.append("Index entries without matching files: ")
            #eliminate matched keys from all keys
            erroneousPaths = [key for key in dict.keys()]
            for correct in correctCount:
                if correct in erroneousPaths:
                    erroneousPaths.remove(correct)
            for item in erroneousPaths:
                print(item)
                testOutput.append(item)
        with open(self.logFile,"a") as log:
            log.write(timestamp + " TEST" + '\n')
            for item in testOutput:
                log.write(item + '\n')
            log.write('\n')
        log.close()







    def get(self,args):
        if args == None:
            print("Error: no filename or pattern given.")
        else:
            matches = []
            for file in os.listdir(self.subdirectory):
                if args in file:
                    matches.append(file)
            if len(matches) == 0:
                print("No matches found.")
            elif len(matches) == 1:
                #restore single file to current dir
                print("Match found:", matches[0])
                dir = os.getcwd()
                shutil.copyfile(os.path.join(self.subdirectory,matches[0]), os.path.join(dir,matches[0]))
                print(matches[0],"restored to",os.path.join(dir,matches[0]))
            else:
                print(len(matches),"matches found")
                count = 0
                for item in matches[:50]:
                    print(count,">>",item)
                    count += 1
                number = int(input("Restore file number: "))
                selection = matches[number]
                #restore selection
                print("You chose", selection)
                dir = os.getcwd()
                shutil.copyfile(os.path.join(self.subdirectory,selection), os.path.join(dir,selection))
                print(selection,"restored to",os.path.join(dir,selection))




    def restore(self,args):
        if args == None:
            args = os.getcwd()
        elif not os.path.isdir(args):
            args = os.path.join(os.getcwd(),args)
            if not os._exists(args):
                os.makedirs(os.path.join(os.getcwd(),args))
        objects = os.listdir(self.subdirectory) #list of all files in myArchive/objects
        indexDict = pickle.load(open(self.indexFile,"rb")) #index file

        #find any 'parent folders' within the stored file paths
        allBasenames = [b for (a,b) in indexDict.values()]
        parentLength = min([len(base.split(os.sep)) for base in allBasenames])
        allMatches = [os.path.basename(p) for p in allBasenames if len(p.split(os.sep)) == parentLength]
        parents = list(set(allMatches))

        for item in objects:
            if not item.startswith('.'):
                originPath = indexDict[item][1]
                #identify if this is a subdirectory / where it needs to be restored to in the tree
                originSplit = originPath.split(os.sep)
                for origin in originSplit:
                    if origin in parents:
                        x = originSplit.index(origin)
                        fileTree = originSplit[x:]
                        restoreTo = os.path.join(*fileTree)
                        #restoreTo = originPath from parent on
                newPath = os.path.join(args,restoreTo)
                if not os.path.isdir(newPath):
                    os.makedirs(newPath)

                shutil.copyfile(os.path.join(self.subdirectory,item), os.path.join(newPath,item))
        print("myArchive restored to directory",args)





    def do_quit(self, args):
        """Quits the program."""
        print("Quitting.")
        raise SystemExit


if __name__ == '__main__':
    prompt = mybackup()
    prompt.prompt = '> '
    prompt.cmdloop('Welcome to mybackup. Available commands are: *init *store [dir] *list [optional pattern] *test *get [filename-or-pattern] *restore [optional-directory]')

