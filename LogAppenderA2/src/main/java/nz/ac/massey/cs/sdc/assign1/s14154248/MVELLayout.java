package nz.ac.massey.cs.sdc.assign1.s14154248;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.spi.LoggingEvent;
import org.mvel2.templates.TemplateRuntime;

public class MVELLayout extends org.apache.log4j.Layout {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	

	@Override
	public String format(LoggingEvent event) {
		String template = "@{p} [@{t}] @{c} @{d} -- @{m} @{n}";
		Map vars = new HashMap();
		vars.put("c", event.categoryName);
		vars.put("d", new Date(event.timeStamp).toString());
		vars.put("m", event.getRenderedMessage());
		vars.put("p", event.getLevel());
		vars.put("t", event.getThreadName());
		vars.put("n", '\n');
		String formatted = (String) TemplateRuntime.eval(template, vars);
		String tester = "Hello World";
		return formatted;
	}

	@Override
	public boolean ignoresThrowable() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void activateOptions() {
		// TODO Auto-generated method stub
		
	}

}
