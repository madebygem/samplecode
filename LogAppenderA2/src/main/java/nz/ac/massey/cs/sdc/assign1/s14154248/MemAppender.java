package nz.ac.massey.cs.sdc.assign1.s14154248;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;

public class MemAppender extends AppenderSkeleton {
	
	private static MemAppender singleMem;
	private static List<LoggingEvent> logs;
	private int maxSize = 10;
	private long discardedLogs = 0;
	
	private MemAppender() {
		this(new ArrayList<LoggingEvent>());
	}
	
	private MemAppender(List<LoggingEvent> list) {
		logs = list;
	}
	
	public static MemAppender getInstance(List<LoggingEvent> list) {
		if (singleMem == null) {
			singleMem = new MemAppender(list);
		}
		else {
			logs = list;
		}
		return singleMem;
	}
	
	public static MemAppender getInstance() {
		if (singleMem == null) {
			singleMem = new MemAppender();
		}
		else {
			List newList = new ArrayList<LoggingEvent>();
			logs = newList;
		}
		return singleMem;
	}

	
	public List<LoggingEvent> getCurrentLogs() {
		List<LoggingEvent> returnLogs = Collections.unmodifiableList(logs);
		return returnLogs;
	}
	
	public long getDiscardedLogCount() {
		return discardedLogs;
	}
	
	public void setMaxLogSize(int x) {
		maxSize = x;
	}

	public void close() {
		// TODO Auto-generated method stub
		
	}

	public boolean requiresLayout() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void append(LoggingEvent event) {
		if (logs.size() == maxSize) {
			discardedLogs += maxSize;
			logs.clear();
		}
		logs.add(event);
	}

	
}
