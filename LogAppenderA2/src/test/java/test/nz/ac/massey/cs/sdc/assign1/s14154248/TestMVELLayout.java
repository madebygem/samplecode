package test.nz.ac.massey.cs.sdc.assign1.s14154248;



import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nz.ac.massey.cs.sdc.assign1.s14154248.MVELLayout;
import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class TestMVELLayout {
	
	Logger fileLogger;
	Logger consoleLogger;
	Appender file1;
	Appender file2;
	Appender console;
	Layout layout;
	LoggingEvent eventOne;
	LoggingEvent eventTwo;
	String testOneDate;
	String testTwoDate;


	@Before
	public void setUp() throws Exception {		
		// test1Log uses a FileAppender with a MVELLayout
		fileLogger = Logger.getLogger("file");
		layout = new MVELLayout();
		file1 = new FileAppender(layout,"firstLogs.txt");
		file2 = new FileAppender(new SimpleLayout(),"secondLogs.txt");
		fileLogger.addAppender(file1);
		fileLogger.addAppender(file2);
		
		consoleLogger = Logger.getLogger("console");
		console = new ConsoleAppender(layout);
		
		
		eventOne = new LoggingEvent("TestMVELLayout.class",consoleLogger,Level.INFO,"This is an info message",null);
		eventTwo = new LoggingEvent("TestMVELLayout.class",consoleLogger,Level.DEBUG,"This is a debug message",null);
		
		testOneDate = new Date(eventOne.timeStamp).toString();
		testTwoDate = new Date(eventTwo.timeStamp).toString();
	}

	@After
	public void tearDown() throws Exception {
		fileLogger = null;
		layout = null;
		file1 = null;
		file2 = null;
		eventOne = null;
		eventTwo = null;
	}


	@Test
	public void testSimpleVsMVEL() {
		// one LoggingEvent from one logger sent to 2 separate files, then read as 
		// strings and compared to show MVEL layout formatting differs from SimpleLayout
		String readFirstLine;
		String readSecondLine;
		fileLogger.debug("This is a debug message");
		try {
            File f = new File("firstLogs.txt");
            File g = new File("secondLogs.txt");
            BufferedReader b = new BufferedReader(new FileReader(f));
            BufferedReader c = new BufferedReader(new FileReader(g));
            readFirstLine = b.readLine();
            readSecondLine = c.readLine();
            assertFalse(readFirstLine.equals(readSecondLine));
        } catch (IOException e) {
            e.printStackTrace();
        }
		
    }
	
	
	@Test
	public void formatEvent() {
		String oneFormatted = layout.format(eventOne);
		String pattern = eventOne.getLevel() + " " + "[" + eventOne.getThreadName() + "] " + eventOne.categoryName + " " + testOneDate + " -- " + eventOne.getRenderedMessage() + " \n";
		assertEquals(oneFormatted,pattern);
	}
	
	@Test
	public void stringFormat() {
		String twoFormatted = layout.format(eventTwo);
		String pattern = String.format("%s [%s] %s %s -- %s %n", "DEBUG", "main", "console", testTwoDate, "This is a debug message");
		assertEquals(twoFormatted,pattern);
	}
	
	@Test
	public void testFileVsConsole() throws IOException {
		// shows that MVELLayout formatting is identical when sent to file and console from
		// identical LoggingEvent and one logger
		String readFirstLine;
		
		final ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final PrintStream printStream = new PrintStream(baos);
		System.setOut(printStream);
		final Logger logger = Logger.getLogger(TestMVELLayout.class);
		logger.addAppender(new ConsoleAppender(layout));
		logger.addAppender(new FileAppender(layout,"tester.txt"));
		logger.debug("This is a debug message");
		final String consoleOutput = baos.toString();
		  
		logger.debug("This is a debug message");
			try {
	            File f = new File("tester.txt");
	            BufferedReader b = new BufferedReader(new FileReader(f));
	            readFirstLine = b.readLine();
	            assertFalse(readFirstLine.equals(consoleOutput));
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}
	

}
