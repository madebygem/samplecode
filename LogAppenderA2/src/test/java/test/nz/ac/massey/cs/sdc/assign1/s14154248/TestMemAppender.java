package test.nz.ac.massey.cs.sdc.assign1.s14154248;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import nz.ac.massey.cs.sdc.assign1.s14154248.MemAppender;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggingEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class TestMemAppender {
	List<LoggingEvent> testList;
	MemAppender testMem;
	Logger memLog;
	

	@Before
	public void setUp() throws Exception {
		testList = new ArrayList<LoggingEvent>();
		testMem = MemAppender.getInstance(testList);
		memLog = Logger.getLogger("MemAppender");
		memLog.addAppender(testMem);
		memLog.setLevel(Level.INFO);
	}

	@After
	public void tearDown() throws Exception {
		testList = null;
		testMem = null;
		memLog = null;
	}
	
	@Test
	public void testNotNull() {
		// MemAppender testMem was initialised in setUp() this is to test an instance was created
		assertFalse(testMem.equals(null));
	}
	
	public void testCorrectAppender() {
		assertEquals(testMem.getName(),"MemAppender");
	}
	
	@Test
	public void testInstanceWithParams() {
		// in setUp(), memLog was passed testList as the parameter
		memLog.error("This is an error message");
		assertEquals(testList.size(),1);
	}
	
	@Test
	public void testInstanceNoParams() {
		testMem = MemAppender.getInstance();
		memLog.info("This is an info message");
		// this info message is NOT added to testList as the instance has been changed (no params = NEW ArrayList)
		assertEquals(testList.size(),0);
	}
	
	@Test
	public void checkCurrentLogs() {
		memLog.fatal("This is a fatal message");
		memLog.error("This is an error message");
		memLog.info("This is an info message");
		// various levels are added to the logs
		assertEquals(testMem.getCurrentLogs().size(),3);
	}
	
	@Test
	public void testDiscardedLogs() {
		// also tests 'setMaxSize' method
		testMem.setMaxLogSize(3);
		memLog.fatal("This is a fatal message");
		memLog.error("This is an error message");
		memLog.info("This is an info message");
		memLog.fatal("This is a fatal message");
		memLog.error("This is an error message");
		assertEquals(testMem.getDiscardedLogCount(),3);
	}

	
	@Test
	public void testDebugLevel() {
		memLog.debug("This is a debug message");
		memLog.warn("This is a warn message");
		// debug message should not have been added to current logs (does not match level)
		assertEquals(testMem.getCurrentLogs().size(),1);
	}
	
	@Test
	public void testSingleton() {
		MemAppender newMem = MemAppender.getInstance();
		assertSame(testMem,newMem);
	}

}
