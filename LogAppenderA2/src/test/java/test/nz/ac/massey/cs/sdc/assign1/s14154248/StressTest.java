package test.nz.ac.massey.cs.sdc.assign1.s14154248;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import nz.ac.massey.cs.sdc.assign1.s14154248.MVELLayout;
import nz.ac.massey.cs.sdc.assign1.s14154248.MemAppender;

import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.spi.LoggingEvent;

public class StressTest {
	
	private static Logger memLogger = Logger.getLogger("memLogger");
	private static Logger layoutLogger = Logger.getLogger("layoutLogger");
	private static MemAppender mem;
	private static List<LoggingEvent> linkedList = new LinkedList<LoggingEvent>();
	private static List<LoggingEvent> arrayList = new ArrayList<LoggingEvent>();
	private static FileAppender fileA;
	private static ConsoleAppender consoleA;


	public static void main(String[] args) throws InterruptedException, IOException {
		StressTest test = new StressTest();
		Thread.sleep(5000);
		System.out.println("Testing: MemAppender with LinkedList");
		test.stressTestMemAppender(linkedList);
		Thread.sleep(5000);
		
		System.out.println("Testing: MemAppender with ArrayList");
		test.stressTestMemAppender(arrayList);
		Thread.sleep(5000);
		
		fileA = new FileAppender(new MVELLayout(),"stresstest.txt");
		System.out.println("Testing: FileAppender with MVELLayout");
		test.stressTestLayout(fileA);
		Thread.sleep(5000);
		
		consoleA = new ConsoleAppender();
		consoleA.setWriter(new PrintWriter(System.out));
		System.out.println("Testing: ConsoleAppender with MVELLayout");
		test.stressTestLayout(consoleA);
	}
	
	public void stressTestMemAppender(List<LoggingEvent> list) throws InterruptedException {
		long startTime = System.currentTimeMillis();
		mem = MemAppender.getInstance(list);
		memLogger.addAppender(mem);
		mem.setMaxLogSize(500);
		this.stress(memLogger);
	}

	
	public void stressTestLayout(Appender appender) throws InterruptedException {
		long startTime = System.currentTimeMillis();
		appender.setLayout(new MVELLayout());
		layoutLogger.addAppender(appender);
		
		this.stress(layoutLogger);
		
		this.vsPatternLayout(appender);
	}
	
	public void vsPatternLayout(Appender appender) throws InterruptedException {
		long startTime = System.currentTimeMillis();
		Logger patternLogger = Logger.getLogger("patternLogger");
		appender.setLayout(new PatternLayout());
		patternLogger.addAppender(appender);
		System.out.println("Testing with PatternLayout");
		this.stress(patternLogger);
	}
	
	public void stress(Logger logger) throws InterruptedException {
		long startTime = System.currentTimeMillis();
		
		int i;
		for (i=0; i<1000000; i++) {
			logger.debug("This is a debug message");
			if (i==500) {
				System.out.println("MemAppender maxSize reached");
			}
		}
		long endTime = System.currentTimeMillis();
		
        Runtime runtime = Runtime.getRuntime();
        // Run the garbage collector
        runtime.gc();
        // Calculate the used memory
        long memory = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Used memory is bytes: " + memory);
		System.out.println("Test took " + (endTime - startTime) + " milliseconds");
	}

}
