# Template for the algorithm to solve a sudoku. Builds a recursive backtracking solution
# that branches on possible values that could be placed in the next empty cell.
# Initial pruning of the recursion tree -
#       we don't continue on any branch that has already produced an inconsistent solution
#       we stop and return a complete solution once one has been found

import pygame, Sudoku_IO
from operator import itemgetter

def solve(snapshot, screen):
    # display current snapshot
    pygame.time.delay(200)
    Sudoku_IO.displayPuzzle(snapshot, screen)
    pygame.display.flip()
    # if current snapshot is complete ... return a value
    # if current snapshot not complete ...
    # for each possible value for an empty cell
    #    clone current snapshot and update it,
    #    if new snapshot is consistent, perform recursive call
    # return a value

    if isComplete(snapshot) and isValid(snapshot):
        return True

    else:
        findSingletons(snapshot)

        snap = snapshot.clone()
        Sudoku_IO.displayPuzzle(snap, screen)
        pygame.time.delay(50)

        forceValues(snap)

        snapshot = snap.clone()
        Sudoku_IO.displayPuzzle(snapshot, screen)
        pygame.time.delay(50)

        matchThree(snapshot)

        snap = snapshot.clone()
        Sudoku_IO.displayPuzzle(snap, screen)
        pygame.time.delay(50)

        #refinePossibilities(snap)

        #snapshot = snap.clone()
        #Sudoku_IO.displayPuzzle(snapshot, screen)
        #pygame.time.delay(50)


        if not isComplete(snap):

            # take the first unsolved cell
            focus = snap.unsolvedCells()[0]

            for value in range(1,10):
                if valFits(snap, focus, value):
                    focus.setVal(value)

                    snapshot = snap.clone()
                    Sudoku_IO.displayPuzzle(snapshot, screen)
                    pygame.time.delay(200)

                    if solve(snapshot, screen):
                        return True
                    else:
                        continue
        # if there was no possible solution for the puzzle:
        return False

# Check whether a snapshot is consistent, i.e. all cell values comply
# with the sudoku rules (each number occurs only once in each block, row and column).

def isValid(snapshot):
    # check rows
    for x in range(9):
        usedVals = []
        for cell in snapshot.cellsByRow(x):
            if cell.getVal() != 0:
                usedVals.append(cell.getVal())
        if sorted(usedVals) != list(set(usedVals)):
            return False
    # check columns
    for x in range(9):
        usedVals = []
        for cell in snapshot.cellsByCol(x):
            if cell.getVal() != 0:
                usedVals.append(cell.getVal())
        if sorted(usedVals) != list(set(usedVals)):
            return False
    # check blocks
    for x in [0,3,6]:
        for y in [0,3,6]:
            usedVals = []
            for cell in snapshot.cellsByBlock(x,y):
                if cell.getVal() != 0:
                    usedVals.append(cell.getVal())
            if sorted(usedVals) != list(set(usedVals)):
                return False
    return True

# Check whether a puzzle is solved.
# return true if the sudoku is solved, false otherwise

def isComplete(snapshot):
    if len(snapshot.unsolvedCells()) == 0:
        return True
    else:
        return False



def findSingletons(snapshot):
    # here I want to check for cells which only have one possible value by inspecting
    # each row, then column, and block.

    # for each currently unsolved cell
    for cell in snapshot.unsolvedCells():
        row = snapshot.cellsByRow(cell.getRow())
        # check each cell in it's row for already used values, and
        # remove them from this cell's possibilities
        for item in row:
            if item.getVal() in cell.getPossibilities():
                cell.removePossibility(item.getVal())


        # then do the same for the column
        column = snapshot.cellsByCol(cell.getCol())
        for item in column:
            if item.getVal() in cell.getPossibilities():
                cell.removePossibility(item.getVal())


        # then check the block
        block = snapshot.cellsByBlock(cell.getRow(), cell.getCol())
        for item in block:
            if item.getVal() in cell.getPossibilities():
                cell.removePossibility(item.getVal())

        # if we are down to one value left, set it
        if len(cell.getPossibilities()) == 1:
            cell.setVal(cell.getPossibilities()[0])


def valFits(snapshot, cell, value):
    for row in snapshot.cellsByRow(cell.getRow()):
        if row.getVal() == value:
            return False
    for col in snapshot.cellsByCol(cell.getCol()):
        if col.getVal() == value:
            return False
    for block in snapshot.cellsByBlock(cell.getRow(),cell.getCol()):
        if block.getVal() == value:
            return False
    return True



def forceValues(snapshot):
    for x in range(9):
        for y in range(1,10):

            possibleCellMatches = []
            usedValues = []
            for cell in snapshot.cellsByRow(x):  # identify the row 0-8
                if cell.getVal() == 0: # if the cell is empty
                    if valFits(snapshot, cell, y): # see if y is a possible fit
                        possibleCellMatches.append(cell)  # if it is, add it to a list of matches
                elif cell.getVal() == y:
                    usedValues.append(y)

            # when we've tried this 1 value in every cell, we'll know if there's only one place it could go
            if y not in usedValues and len(possibleCellMatches) == 1:
                cellMatch = possibleCellMatches[0]  # identify the cell it must fit in
                cellMatch.setVal(y) # and set the value

            #check the column in the same manner
            possCellMatches = []
            usedVals = []
            for cell in snapshot.cellsByCol(x):
                if cell.getVal() == 0:
                    if valFits(snapshot, cell, y):
                        possCellMatches.append(cell)
                elif cell.getVal() == y:
                    usedVals.append(y)
            if y not in usedVals and len(possCellMatches) == 1:
                cellMatch = possCellMatches[0]
                cellMatch.setVal(y)

    # now check the block
    for a in [0,3,6]:
        for b in [0,3,6]:
            for c in range(1,10):
                potentialCellMatches = []
                uValues = []
                for cell in snapshot.cellsByBlock(a,b):
                    if cell.getVal() == 0:
                        if valFits(snapshot, cell, c):
                            potentialCellMatches.append(cell)
                    elif cell.getVal() == c:
                        uValues.append(c)
                if c not in uValues and len(potentialCellMatches) == 1:
                    cellMatch = potentialCellMatches[0]
                    cellMatch.setVal(c)


def matchThree(snapshot):

    # first look at the sets of 3 blocks horizontally

    blocksA = snapshot.cellsByRow(0) + snapshot.cellsByRow(1) + snapshot.cellsByRow(2)
    blocksB = snapshot.cellsByRow(3) + snapshot.cellsByRow(4) + snapshot.cellsByRow(5)
    blocksC = snapshot.cellsByRow(6) + snapshot.cellsByRow(7) + snapshot.cellsByRow(8)

    for block in [blocksA, blocksB, blocksC]:
        cellValues = {}
        for cell in block:
            if cell.getVal() != 0:
                if cell.getVal() not in cellValues.keys():
                    cellValues[cell.getVal()] = [(cell.getRow(),cell.getCol())]
                else:
                    cellValues[cell.getVal()].append((cell.getRow(),cell.getCol()))

        for key in cellValues.keys():
            if len(cellValues[key]) == 2:
                cellA = cellValues[key][0]
                cellB = cellValues[key][1]
                if block == blocksA:
                    r = [0,1,2]
                elif block == blocksB:
                    r = [3,4,5]
                else:
                    r = [6,7,8]
                for row in r:
                    if cellA[0] != row and cellB[0] != row:
                        cellRow = row

                block1 = range(0,3)
                block2 = range(3,6)
                block3 = range(6,9)
                if cellA[1] not in block1 and cellB[1] not in block1:
                    cellCol = block1
                elif cellA[1] not in block2 and cellB[1] not in block2:
                    cellCol = block2
                else:
                    cellCol = block3

                couldFit = []
                for square in snapshot.cellsByRow(cellRow):
                    if square.getCol() in cellCol:
                        if valFits(snapshot, square, key):
                            couldFit.append(square)
                if len(couldFit) == 1:
                    couldFit[0].setVal(key)

    # now look at the 3 sets of vertical blocks

    blocksD = snapshot.cellsByCol(0) + snapshot.cellsByCol(1) + snapshot.cellsByCol(2)
    blocksE = snapshot.cellsByCol(3) + snapshot.cellsByCol(4) + snapshot.cellsByCol(5)
    blocksF = snapshot.cellsByCol(6) + snapshot.cellsByCol(7) + snapshot.cellsByCol(8)

    for block in [blocksD, blocksE, blocksF]:
        cellVals = {}
        for cell in block:
            if cell.getVal() != 0:
                if cell.getVal() not in cellVals.keys():
                    cellVals[cell.getVal()] = [(cell.getRow(),cell.getCol())]
                else:
                    cellVals[cell.getVal()].append((cell.getRow(),cell.getCol()))

        for key in cellVals.keys():
            if len(cellVals[key]) == 2:
                cellA = cellVals[key][0]
                cellB = cellVals[key][1]
                if block == blocksD:
                    r = [0,1,2]
                elif block == blocksE:
                    r = [3,4,5]
                else:
                    r = [6,7,8]
                for col in r:
                    if cellA[1] != col and cellB[1] != col:
                        cellCol = col

                block4 = range(0,3)
                block5 = range(3,6)
                block6 = range(6,9)
                if cellA[0] not in block4 and cellB[0] not in block4:
                    cellRow = block4
                elif cellA[0] not in block5 and cellB[0] not in block5:
                    cellRow = block5
                else:
                    cellRow = block6

                couldFit = []
                for square in snapshot.cellsByCol(cellCol):
                    if square.getRow() in cellRow:
                        if valFits(snapshot, square, key):
                            couldFit.append(square)
                if len(couldFit) == 1:
                    couldFit[0].setVal(key)



def refinePossibilities(snapshot):
    for a in range(9):
        doublePossibilities = []
        for cell in snapshot.cellsByRow(a):
            counter = []
            for x in range(1,10):
                if valFits(snapshot, cell, x):
                    counter.append(x)
            if len(counter) == 2:
                doublePossibilities.append((cell, counter[0], counter[1]))

        if len(doublePossibilities) > 1:

            matchCells = sorted(doublePossibilities,key=itemgetter(1))

            matchFound = []
            removeValues = []
            while len(matchCells) != 0:
                checker = matchCells.pop(0)
                for cellMatch in matchCells:
                    if cellMatch[1] == checker[1] and cellMatch[2] == checker[2]:
                        matchFound.append(checker)
                        matchFound.append(cellMatch)
                        removeValues.append(checker[1])
                        removeValues.append(checker[2])

            for row in snapshot.cellsByRow(a):
                if row not in matchFound:
                    for item in removeValues:
                        if item in row.getPossibilities():
                            row.removePossibility(item)
                if len(row.getPossibilities()) == 1:
                    row.setVal(row.getPossibilities()[0])

    for a in range(9):
        doublePossibilities = []
        for cell in snapshot.cellsByCol(a):
            counter = []
            for x in range(1,10):
                if valFits(snapshot, cell, x):
                    counter.append(x)
            if len(counter) == 2:
                doublePossibilities.append((cell, counter[0], counter[1]))

        if len(doublePossibilities) > 1:

            matchCells = sorted(doublePossibilities,key=itemgetter(1))

            matchFound = []
            removeValues = []
            while len(matchCells) != 0:
                checker = matchCells.pop(0)
                for cellMatch in matchCells:
                    if cellMatch[1] == checker[1] and cellMatch[2] == checker[2]:
                        matchFound.append(checker)
                        matchFound.append(cellMatch)
                        removeValues.append(checker[1])
                        removeValues.append(checker[2])

            for col in snapshot.cellsByCol(a):
                if col not in matchFound:
                    for item in removeValues:
                        if item in col.getPossibilities():
                            col.removePossibility(item)
                if len(col.getPossibilities()) == 1:
                    col.setVal(col.getPossibilities()[0])