-- Gemma Ward 14154248
module Reversi where

import Data.List

-- Position type and utility functions
type Position = (Int, Int)

-- ***
-- Given a Position value, determine whether or not it is a legal position on the board
isValidPos :: Position -> Bool
isValidPos (x, y)
    | x `elem` cells && y `elem` cells = True
    | otherwise = False
    where cells = [0..7]


-- Player type and utility functions
data Player = PlayerWhite | PlayerBlack deriving (Eq)
instance Show Player where
	show PlayerWhite = "white"
	show PlayerBlack  = "black"

-- ***
-- Given a Player value, return the opponent player
otherPlayer :: Player -> Player
otherPlayer PlayerWhite = PlayerBlack
otherPlayer PlayerBlack = PlayerWhite

-- Piece type and utility functions
data Piece = Piece Position Player deriving (Eq)
instance Show Piece where
	show (Piece _ PlayerWhite) = " W"
	show (Piece _ PlayerBlack) = " B"

-- ***
-- Given a Player value and a Piece value, does this piece belong to the player?
isPlayer :: Player -> Piece -> Bool
isPlayer p (Piece _ player)
    | player == p = True
    | otherwise = False

-- ***
-- Given a Piece value, determine who the piece belongs to
playerOf :: Piece -> Player
playerOf (Piece _ p) = p 

-- ***
-- Flip a piece over
flipPiece :: Piece -> Piece
flipPiece (Piece pos play) = (Piece pos (otherPlayer play))


-- Board type and utility functions
type Board = [Piece]

-- The initial configuration of the game board
initialBoard :: Board
initialBoard =
	[
		Piece (3,4) PlayerWhite, Piece (4,4) PlayerBlack,
		Piece (3,3) PlayerBlack, Piece (4,3) PlayerWhite
	]

-- ***
-- Given a Position value, is there a piece at that position?
isOccupied :: Position -> Board -> Bool
isOccupied pos b
    | pos `elem` usedPos = True 
    | otherwise = False
    where usedPos = [x | (Piece x _ ) <- b]


-- ***
-- Which piece is at a given position? 
-- Return Nothing in the case that there is no piece at the position
-- Otherwise return Just the_piece

pieceAt :: Position -> Board -> Maybe Piece
pieceAt pos b 
    | length match == 0 = Nothing
    | otherwise = Just (head match)
    where match = [(Piece x y) | (Piece x y) <- b, x == pos]


-- ***
-- Determine if a particular piece can be placed on a board.  
-- There are two conditions: 
-- (1) no two pieces can occupy the same space, and 
-- (2) at least one of the other player's pieces must be flipped by the placement of the new piece.
validMove :: Piece -> Board -> Bool
validMove (Piece x y) b 
    | isOccupied x b == True = False
    | length (toFlip (Piece x y) b) == 0 = False
    | otherwise = True

-- ***
-- Determine which pieces would be flipped by the placement of a new piece
toFlip :: Piece -> Board -> [Piece]
toFlip (Piece pos p) b = flippable (otherPlayer p) (getLineDir (1,1) (Piece pos p) b) ++ flippable (otherPlayer p) (getLineDir (1,0) (Piece pos p) b) ++ flippable (otherPlayer p) (getLineDir (-1,1) (Piece pos p) b) ++ flippable (otherPlayer p) (getLineDir (0,1) (Piece pos p) b) ++ flippable (otherPlayer p) (getLineDir (1,-1) (Piece pos p) b) ++ flippable (otherPlayer p) (getLineDir (0,-1) (Piece pos p) b) ++ flippable (otherPlayer p) (getLineDir (-1,-1) (Piece pos p) b) ++ flippable (otherPlayer p) (getLineDir (-1,0) (Piece pos p) b)



-- ***
-- Auxillary function for toFlip. 
-- You don't have to use this function if you prefer to define toFlip some other way.
-- Determine which pieces might get flipped along a particular line 
-- when a new piece is placed on the board.  
-- The first argument is a vector (pair of integers) that describes 
-- the direction of the line to check.  
-- The second argument is the hypothetical new piece.  
-- The return value is either the empty list, 
-- a list where all pieces belong to the same player, 
-- or a list where the last piece belongs to the player of the hypothetical piece.  
-- Only in the last case can any of the pieces be flipped.

getLineDir :: (Int, Int) -> Piece -> Board -> [Piece]
getLineDir (d1,d2) (Piece (x,y) play) b 
    | isOccupied (x+d1,y+d2) b == False = []
    | otherwise = match ++ getLineDir (d1,d2) (head match) b
    where match = [(Piece (a,b) p) | (Piece (a,b) p) <- b, a == x+d1 && b == y+d2]  

-- ***
-- Auxillary function for toFlip.
-- You don't have to use this function if you prefer to define toFlip some other way.
-- Given the output from getLineDir, determine which, if any, of the pieces would be flipped.

flippable :: Player -> [Piece] -> [Piece]
flippable _ [] = []
flippable x (p:[]) = []
flippable x (p:ps)
    | length black == length (p:ps) || length white == length (p:ps) = []
    | playerOf p /= x = []
    | otherwise = [p] ++ flippable x ps
    where black = [pc | pc <- (p:ps), playerOf pc == PlayerBlack]
          white = [pc | pc <- (p:ps), playerOf pc == PlayerWhite]
-- if all elements in the list belong to one player or the other


-- ***
-- Place a new piece on the board.  Assumes that it constitutes a validMove
makeMove :: Piece -> Board -> Board
makeMove p b = map flipPiece flippers ++ [pce | pce <- b, not (pce `elem` flippers)] ++ [p]
    where flippers = toFlip p b

-- ***
-- Find all valid moves for a particular player
allMoves :: Player -> Board -> [Piece]
allMoves _ [] = []
allMoves p b = [(Piece pos p) | pos <- neighbours, validMove (Piece pos p) b == True]
    where neighbours = nub (findNeighbours b)
    
    
    
getPos :: Piece -> Position 
getPos (Piece pos _) = pos
    
findNeighbours :: Board -> [Position]
findNeighbours [] = []
findNeighbours (b:[]) = neighbours (getPos b)
findNeighbours (b:bs) = neighbours (getPos b) ++ findNeighbours bs

neighbours :: Position -> [Position]
neighbours (x,y) = [pos | pos <- positions, isValidPos pos == True]
    where positions = [(x-1,y-1),(x-1,y),(x-1,y+1),(x,y-1),(x,y+1),(x+1,y-1),(x+1,y),(x+1,y+1)]

-- ***
-- Count the number of pieces belonging to a player
score :: Player -> Board -> Int
score p b = length scoreCounter
    where scoreCounter = [(Piece x y) | (Piece x y) <- b, y == p]

-- ***
-- Decide whether or not the game is over. The game is over when neither player can make a validMove
isGameOver :: Board -> Bool
isGameOver b 
    | length (allMoves PlayerWhite b) == 0 && length (allMoves PlayerBlack b) == 0 = True
    | otherwise = False

-- ***
-- Find out who wins the game.  
-- Return Nothing in the case of a draw.
-- Otherwise return Just the_Player
winner :: Board -> Maybe Player
winner b 
    | (score PlayerBlack b) > (score PlayerWhite b) = Just PlayerBlack
    | (score PlayerBlack b) < (score PlayerWhite b) = Just PlayerWhite
    | otherwise = Nothing 


